# Simple React + React-Redux blog

This React project was created whilst learning React and uses a third party webservice which might cause issues.

This project is a simple blog demonstating different routes and views in React. It allows the user to add,edit,view and remove posts.

### To get started:
1. Run command `npm install`
2. Run command `npm start`
    - This will start the dev server
3. Open browser and navigate to http://localhost:8080    

